<?php
/**
 * @file
 * free_og.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function free_og_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function free_og_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function free_og_node_info() {
  $items = array(
    'custom_group' => array(
      'name' => t('Custom Group'),
      'base' => 'node_content',
      'description' => t('Content type for adding custom organic group'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
