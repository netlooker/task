<?php
/**
 * @file
 * free_og.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function free_og_default_rules_configuration() {
  $items = array();
  $items['rules_free_groups_registration'] = entity_import('rules_config', '{ "rules_free_groups_registration" : {
      "LABEL" : "Free groups registration",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "views_bulk_operations" ],
      "ON" : { "user_insert" : [] },
      "IF" : [
        { "entity_is_of_type" : { "entity" : [ "account" ], "type" : "user" } }
      ],
      "DO" : [
        { "views_bulk_operations_action_load_id_list" : {
            "USING" : { "view" : "list_of_free_groups|default" },
            "PROVIDE" : { "entity_id_list" : { "list_of_free_groups" : "List of free groups" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "list-of-free-groups" ] },
            "ITEM" : { "list_item" : "List item" },
            "DO" : [
              { "entity_fetch" : {
                  "USING" : { "type" : "node", "id" : [ "list-item" ] },
                  "PROVIDE" : { "entity_fetched" : { "free_group" : "Free group" } }
                }
              },
              { "component_rules_set_user_as_a_free_groups_subscriber" : { "og_node" : [ "free-group" ], "user" : [ "account" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_set_user_as_a_free_groups_subscriber'] = entity_import('rules_config', '{ "rules_set_user_as_a_free_groups_subscriber" : {
      "LABEL" : "Set user as a free groups subscriber",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "og" ],
      "USES VARIABLES" : {
        "og_node" : { "label" : "og_node", "type" : "node" },
        "user" : { "label" : "user", "type" : "user" }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "og-node" ],
            "type" : "node",
            "bundle" : { "value" : { "custom_group" : "custom_group" } }
          }
        }
      ],
      "DO" : [
        { "og_subcribe_user" : { "user" : [ "user" ], "group" : [ "og-node" ] } }
      ]
    }
  }');
  return $items;
}
